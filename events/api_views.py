from django.http import JsonResponse
import json
from commons.encoders import ModelEncoder
from .models import Conference, Location, State
from .acls import get_pic, get_weather
from django.views.decorators.http import require_http_methods


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.name}


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]

    encoders = {"location": LocationListEncoder()}

    def get_extra_data(self, o):
        weather = get_weather(
            city=o.location.city, state=o.location.state.name
        )
        return {"weather": weather}


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            conferences, encoder=ConferenceListEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            content["location"] = Location.objects.get(
                name=content["location"]
            )
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Location doesn't exist"},
                status=400,
            )
        conference = Conference.objects.conference_create(content)
        return JsonResponse(
            conference, encoder=ConferenceDetailEncoder, safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, pk):
    if request.method == "GET":
        conference = Conference.objects.get(pk=pk)
        return JsonResponse(
            conference, encoder=ConferenceDetailEncoder, safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            content["location"] = Location.objects.get(
                location=content["location"]
            )
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Location doesn't exist"},
                status=400,
            )
        Conference.objects.filter(pk=pk).update(**content)
        conference = Conference.objects.get(id=pk)
        return JsonResponse(
            conference, encoder=ConferenceDetailEncoder, safe=False
        )
    else:
        count, _ = Conference.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(locations, encoder=LocationListEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            content["state"] = State.objects.get(name=content["state"])
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid State name input"},
                status=400,
            )
        content["picture_url"] = get_pic(
            city=content["city"], state=content["state"]
        )
        location = Location.objects.location_create(**content)
        return JsonResponse(
            location, encoder=LocationDetailEncoder, safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_location(request, pk):
    location = Location.objects.filter(id=pk)
    if request.method == "GET":
        location = Location.objects.get(id=pk)
        return JsonResponse(
            location, encoder=LocationDetailEncoder, safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            content["state"] = State.objects.get(name=content["state"])
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid State name input"}, status=400
            )
        location.update(**content)
        location = Location.objects.get(id=pk)
        return JsonResponse(
            location, encoder=LocationDetailEncoder, safe=False
        )
    else:
        count, _ = location.delete()
        return JsonResponse({"delete": count > 0}, status=400)
