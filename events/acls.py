import requests
import random
from commons.api_keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_pic(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": [f"{city}", "city", "streets", f"{state}"],
        "size": "medium",
        "per_page": "3",
    }
    res = requests.get(url, headers=headers, params=params)
    picture_url = res.json()["photos"][random.randint(0, 2)]["src"]["medium"]
    return picture_url


def get_coord(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": [f"{city}, {state}"],
        "appid": f"{OPEN_WEATHER_API_KEY}",
        "limit": "1",
    }
    try:
        res = requests.get(url, params=params)
        lon = res.json()[0]["lon"]
        lat = res.json()[0]["lat"]
    except IndexError:
        lon = "invalid query"
        lat = "invalid query"
    coordinates = {"lon": lon, "lat": lat}
    return coordinates


def get_weather(city, state):
    url = "https://api.openweathermap.org/data/2.5/weather?"
    coordinates = get_coord(city, state)
    params = {
        "lat": f"{coordinates.get('lat')}",
        "lon": f"{coordinates.get('lon')}",
        "appid": f"{OPEN_WEATHER_API_KEY}",
        "units": "imperial",
    }
    res = requests.get(url, params=params)
    res_dict = res.json()
    print(res_dict)
    weather = {
        "feels_like": res_dict["main"]["feels_like"],
        "humidity": res_dict["main"]["humidity"],
        "temp": res_dict["main"]["temp"],
    }
    return weather
