from django.db import models
from django.urls import reverse


class EventQuerySet(models.QuerySet):
    def test(self):
        pass


class EventManager(models.Manager):
    def get_queryset(self):
        return EventQuerySet(self.model, using=self._db)

    def get_api_url(self):
        params = {
            "Location": "location",
            "Conference": "conference",
        }
        class_name = self.__class__.__name__
        if class_name in params:
            view = params.get(class_name)
            return reverse(f"api_show_{view}", pk=self.pk)
        else:
            return "None"

    def location_create(self, content_dict):
        return self.create(**content_dict)

    def conference_create(self, content_dict):
        return self.create(**content_dict)
