from django.db import models
from django.urls import reverse


class PresentationQuerySet(models.QuerySet):
    pass


class PresentationManager(models.Manager):
    def get_queryset(self):
        return PresentationQuerySet(self.model, using=self._db)

    def get_api_url(self):
        params = {"Presentation": "presentation"}
        class_name = self.__class__.__name__
        if class_name in params:
            view = params.get(class_name)
            return reverse(f"api_show_{view}", pk=self.pk)
        else:
            return None
