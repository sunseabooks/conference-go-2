from django.db import models
from django.urls import reverse


class AttendeeQuerySet(models.QuerySet):
    pass


class AttendeeManager(models.Manager):
    def get_queryset(self):
        return AttendeeQuerySet(self.model, using=self._db)

    def get_api_url(self):
        params = {"Attendee": "attendee"}
        class_name = self.__class__.__name__
        if class_name in params:
            view = params.get(class_name)
            return reverse(f"api_show_{view}", pk=self.pk)
        else:
            return None

    def create_badge(self, model):
        if hasattr(self, "badge"):
            print("Has badge")
        else:
            model.objects.create(attendee=self)
